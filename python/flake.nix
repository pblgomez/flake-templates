{
  description = "A python template";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };
  outputs =
    { self, nixpkgs }:
    let
      name = "test";
      version = "0.0.1";
      pythonVersion = "313";
      pythonDeps =
        p: with p; [
          prometheus_client
          requests
        ];
      commonDeps = pkgs: [ pkgs.hello ];
      # To use unFree packages
      # forEachSystem = function: nixpkgs.lib.genAttrs nixpkgs.lib.systems.flakeExposed ( system: function ( import nixpkgs { inherit system; config.allowUnfree = true; }));
      forEachSystem = function: nixpkgs.lib.genAttrs nixpkgs.lib.systems.flakeExposed (system: function nixpkgs.legacyPackages.${system});
    in
    rec {
      devShells = forEachSystem (pkgs: {
        default = pkgs.mkShellNoCC { packages = commonDeps pkgs ++ [ (pkgs."python${pythonVersion}".withPackages pythonDeps) ]; };
      });
      packages = forEachSystem (pkgs: {
        default = pkgs."python${pythonVersion}Packages".buildPythonPackage {
          pname = name;
          inherit version;
          src = ./src;
          propagatedBuildInputs = pythonDeps pkgs."python${pythonVersion}".pkgs;
          buildInputs = commonDeps pkgs;
        };
      });
      oci = forEachSystem (pkgs: {
        default = pkgs.dockerTools.buildLayeredImage {
          inherit name;
          tag = version; # or "latest"
          created = "now";
          contents = [ packages.${pkgs.system}.default ] ++ commonDeps pkgs;
          config = {
            Cmd = [ "${packages.${pkgs.system}.default}/bin/main.py" ];
            Env = [ "PYTHONUNBUFFERED=1" ];
          };
        };
      });
    };
}
