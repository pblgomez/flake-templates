{
  description = "Collection of flakes";

  outputs = { self }: {
    templates = {

      local = {
        description = "Use local dev shell";
        path = ./local;
      };

      python = {
        description = "Create a python environment with libraries";
        path = ./python;
      };

      nur = {
        description = "Use packages from nixpkgs";
        path = ./nur;
      };

      shell = {
        description = "Use packages from nixpkgs";
        path = ./shell;
      };

      defaultTemplate = self.templates.shell;
    };
  };
}
