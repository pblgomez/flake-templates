# Initializa templates by executing one of the following
```
nix flake init --template gitlab:pblgomez/flake-templates#local
nix flake init --template gitlab:pblgomez/flake-templates#python
```

# Input URLs

I use shorthand for the input urls.

`systems.url = "systems";` -> `github:nix-systems/default`

`nixpkgs.url = "nixpkgs";` -> `github:NixOS/nixpkgs/nixpkgs-unstable`

`nur.url = "nur";` -> `github:nix-community/NUR`

