{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };
  outputs =
    { self, nixpkgs }:
    let
      # To use unFree packages
      # forEachSystem = function: nixpkgs.lib.genAttrs nixpkgs.lib.systems.flakeExposed ( system: function ( import nixpkgs { inherit system; config.allowUnfree = true; }));
      forEachSystem = function: nixpkgs.lib.genAttrs nixpkgs.lib.systems.flakeExposed (system: function nixpkgs.legacyPackages.${system});
    in
    {
      devShells = forEachSystem (pkgs: {
        default = pkgs.mkShellNoCC { packages = with pkgs; [ (callPackage ./default.nix { }) ]; };
      });
    };
}
